var app = angular.module('webChat', [
  'ngRoute',
  'ngResource',
  'base64'
]);

app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when("/", {templateUrl: "partials/home.html", controller: "HomeController"})
    .when("/chat", {templateUrl: "partials/chat.html", controller: "ChatController"})
    .otherwise("/", {templateUrl: "partials/home.html", controller: "HomeController"});
}]);

app.controller('HomeController', function ($base64, $scope, $location, $resource, $rootScope, $http) {
  $scope.login = function(user)
  {
    // This pass has been hardcore as I explained in the project section on GitHub
    $rootScope.token = $base64.encode(user.username + ':' + 'pass');
    $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.token;
    var users = $resource('/api/login');
    users.get(
      function (data) {
        if (data.response == "success") {
          $location.path("/chat");
        }
      }
    );
  };
});

app.controller('ChatController', function ($rootScope, $scope, $location, $resource, $interval) {
  var users = $resource('/api/users');
  $scope.users = users.query();
  
  $interval(function(){
    $scope.users = users.query();
    $scope.getMessages();
  }.bind(this), 5000); 

  $scope.selected = function(user)
  {
    $scope.selectedUser = user;
    $scope.getMessages(user.id);
  }

  $scope.getMessages = function()
  {
    if($scope.selectedUser != undefined)
    {
      var messages = $resource('/api/messages/:fromId', { fromId : $scope.selectedUser.id});
      $scope.messages = messages.query();
    }
  }

  $scope.sendMessage = function(message)
  {
    if($scope.selectedUser != undefined)
    {
      var messages = $resource('/api/messages/:toId/:text', { toId: $scope.selectedUser.id, text: message.text });
      messages.save();
    }
    $scope.message.text = null;
  }

});