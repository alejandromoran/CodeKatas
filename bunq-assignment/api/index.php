<?php

require '../vendor/autoload.php';

use \Slim\Middleware\HttpBasicAuthentication\PdoAuthenticator;

$pdo = new \PDO("sqlite:webchat.sqlite");
$app = new \Slim\Slim();

$env = $app->environment();

$app->add(
	new \Slim\Middleware\HttpBasicAuthentication([
	    "secure" => false,
	    "relaxed" => ["localhost", "178.62.68.75"],
	    "authenticator" => new PdoAuthenticator([
	        "pdo" => $pdo
	    ])
	]));

function getId($pdo)
{
	$statement = $pdo->prepare("SELECT id FROM users WHERE username = :username");
	$statement->bindParam('username', $_SERVER['PHP_AUTH_USER']);
	$statement->execute();
	return $statement->fetch(PDO::FETCH_OBJ)->id;
}

$app->get('/login', function () {
    echo json_encode(array("response"=>"success"));
});

$app->get('/users', function () use ($pdo) {
	$statement = $pdo->prepare("SELECT id,username FROM users WHERE id != (SELECT id FROM users WHERE username = :auth_user)");
	$statement->bindParam('auth_user', $_SERVER['PHP_AUTH_USER']);
	$statement->execute();
	echo json_encode($statement->fetchAll(PDO::FETCH_OBJ));
});

$app->get('/messages/:fromId', function ($fromId) use ($pdo) {
	$statement = $pdo->prepare("SELECT message,datetime,(SELECT username FROM users WHERE id = 'messages'.'from') as 'sender' FROM 'messages'
								WHERE 'messages'.'from' = :fromId AND 'messages'.'to' = :userId OR 'messages'.'to' = :fromId AND 'messages'.'from' = :userId");
	$statement->bindParam('fromId', $fromId);
	$statement->bindParam('userId', getId($pdo));
	$statement->execute();
	echo json_encode($statement->fetchAll(PDO::FETCH_OBJ));
});

$app->post('/messages/:toId/:text', function ($toId, $text) use ($pdo){
	$statement = $pdo->prepare("INSERT INTO messages ('from', 'to', 'message', 'datetime') VALUES (:userId,:toId,:text,:datetime)");
	$statement->bindParam('userId', getId($pdo));
	$statement->bindParam('toId', $toId);
	$statement->bindParam('text', $text);
	$statement->bindParam('datetime', date("Y-m-d H:i:s"));
	if($statement->execute())
	{
		echo json_encode(array("response"=>"success"));
	}
	else
	{
		echo json_encode(array("response"=>"error"));
	}
});

$app->run();