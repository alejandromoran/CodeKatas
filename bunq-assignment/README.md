# WebChat
Simple web chat made with AngularJS as front-end framework and SlimPHP as backend-framework with a SQLite database

<h4>Install</h4>
<ol>
  <li>Clone the project</li>
  <li>Install composer dependencies (composer install)</li>
</ol>

<h4>Design considerations</h4>
<ul>
  <li>As login/register system was not part of the scope of this project, password checks have been hardcoded and just username is being checked</li>
  <li>Basic HTTP Authentication has been implemented with SlimPHP</li>
  <li>HTTP requests should fill the header with "Authorization = Basic + token"</li>
  <li>Auth token for headers is formed by base64 encoding of "username:password"</li>
  <li>Remember that password has been hardcoded in this case and is "pass"</li>
</ul>

# Architecture
![architecture](https://cloud.githubusercontent.com/assets/1541798/10653508/64614efe-7860-11e5-85ed-2a02f6555f01.png)

# API Routes

<ul>
  <li>GET - /login</li>
  <li>GET - /users</li>
  <li>GET - /messages/:fromId</li>
  <li>POST - /messages/:toId/:text</li>
</ul>

# Example of use

<ol>
  <li>Jane logs into the system (/api/login)</li>
  <li>Jane receives the updated users list (/api/users)</li>
  <li>Jane select the user Michael to open the chat history with that user (/api/messages/4)</li>
  <li>Jane sends a message to Michael (/api/messages/4/Hi%20Michael!%20I'm%20Jane!)</li>
</ol>
