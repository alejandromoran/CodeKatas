# This solution is only compatible with Python 3 version

import time

t = time.process_time()
i = 1000
j = 1000
minJ = 100
highest = 0
while i > minJ:
	while j > minJ:
		result = i * j
		reverseResult = str(result)[::-1]
		result2 = str(result)
		if result2 == reverseResult:
			minJ = j
			if result > highest:
				highest = result
		j = j - 1
	j = 1000
	i = i - 1

print (highest)
	
elapsed_time = time.process_time() - t
print (elapsed_time)
