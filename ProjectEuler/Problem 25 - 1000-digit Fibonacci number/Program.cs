﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace _1000_digit_Fibonacci_number
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger last = new BigInteger(0);
            long digitsNumber = last.ToString().Length;
            BigInteger i = new BigInteger(1);
            int cont = 1;
            while (digitsNumber != 1000)
            {
                BigInteger aux = last;
                last = i;
                i = last + aux;
                digitsNumber = i.ToString().Length;
                cont++;
            }

            Console.Write(cont);
            Console.ReadLine();
        }
    }
}
